/*
 * Program do wyswietlania loga e-moto na matrycy DFRobot Gravity - panel z matrycą 8x16 128 LED I2C
 * Copyright by: Wiktor Polek
 * Date: 04.12.2021
 * 
 */

#include<Wire.h>

/* Ekran po lewej to Second _Panel, ten po prawej to First_Panel
 * Lewy wpiety do i2c1 na teensy, prawy do i2c0
 * 
 * W jaki sposob tworzony napis?
 * Numeracja pikseli idzie od Drugiego panelu:
 * 
 * Second_Panel                     First_Panel
 * 16.0       16.7                  16.0       16.7
 * .          .                      .          .
 * .          .                      .          .
 * .          .                      .          .
 * 0.0 . . . 0.7                    0.0 . . . 0.7  
 * 
 * Napis jest zapisany w postaci tablicy 2-bajtowych liczb (dlatego stosowany hex). 
 * Kazda wartos odpowiada zapalonym diodom w kolumnie.
 * 0x000F odpowiada wypelnieniu pierwszych 4 bitow jedynkami, czyli zapalenie pierwszyh 4 diod od dolu.
 * Dla 0xF000 zapala sie wszystkie 4 diody na gorze kolumny.
 * 
 */

//Define okreslajace panele
#define First_Panel 0x00
#define Second_Panel 0x01

//Define z biblioteki
#define _RGBAddr 0x10
#define FUNC 0x02
#define COLOR 0x03
#define PIX_X 0x04
#define PIX_Y 0x05
#define BITMAP 0x06
#define STR 0x07

#define UNCLEAR 0x0
#define CLEAR 0x1
#define Left (0x0 << 1)
#define Right (0x1 << 1)
#define None (0x11)
#define UNSCROLL (0x0 << 2)
#define SCROLL (0x1 <<2)
#define PIX_ENABLE (0x01 << 3)
#define BITMAP_ENABLE (0x10 << 3)
#define STR_ENABLE (0x11 << 3)

#define QUENCH 0
#define RED 1
#define GREEN 2
#define YELLOW 3
#define BLUE 4
#define PURPLE 5
#define CYAN 6
#define WHITE 7

#define SIZE 50

unsigned char buf[SIZE]; //Bufor do przechowywania danych do wyslania
int e_moto_logo[110]; // Tablica zawierajaca wartosci pikseli 
int tab_index=0; // Indeks do wyswietlania elementow tablicy

void create_e_moto_logo(); // Funkcja do wypelnienia tablicy
void clear(int nr_panel); // Funkcja do wyczyszczenia panelu
void  scroll(unsigned char dir); //Przesuwanie napisu - niewykorzystywane 
void  display(unsigned char picIndex,unsigned char color,int nr_panel); // Wyswietlenie obrazka z pamieci matrycy - niewykorzystywane
void  print(int val,unsigned char color,int nr_panel); // Wyswietlenie wartosci - niewykorzystywane
void  print(String s,unsigned char color,int nr_panel); // Wyswietlenie napisu - niewykorzystywane
void  pixel(unsigned char x,unsigned char y,unsigned char color,int nr_panel); // Wyswietlenie pojedynczego piksela
void  fillScreen(unsigned char color,int nr_panel); // Podswietlenie matryy na jeden kolor
int * readReg0(uint8_t addr,uint8_t num); // Funkcje do odczytu  - I2C0, panel0 - po prawej
void  setReg0(unsigned char Reg ,unsigned char *pdata, unsigned char datalen ); // Funkcje do wyslania danych - I2C0, panel0 - po prawej
int * readReg1(uint8_t addr,uint8_t num); // Funkcja do odczytu - I2C1, panel1 - po lewej
void  setReg1(unsigned char Reg ,unsigned char *pdata, unsigned char datalen ); // Funkcje do wyslania danych - I2C1, panel1 - po lewej

void E_moto_print(); // Funkcja do printowania napisu

void setup() 
{
  //Nawiazanie komunikacji po i2c0 i i2c1
  Wire.begin(); 
  Wire1.begin();
  delay(1);
  // Wyczyszczenie paneli
  clear(First_Panel);
  delay(1);
  clear(Second_Panel);
  //Utworzenie tablicy z napisem
  create_e_moto_logo();
}

void loop() 
 {

 E_moto_print();
   delay(10);
}

void E_moto_print() // Funkcja od rysowania napisu
{
  int i,j, color;
  int z; // zmienna do koloru
 
  /*
  if (tab_index > 40 && tab_index < 71) color = GREEN; // Po 40 zmien kolor na zielony
  else if (tab_index > 70 && tab_index < 92) color = RED; // Po  70 zmien kolor na czerwony
  else color = WHITE; //Inaczej bialy
  */ 
  //Petla dla matrycy drugiej - po prawej stronie
  for(i=0; i<8; i++) //Zmiana kolumn
    {
      for(j = 0; j<16; j++) //Zmiana wierszy
        {
          if (tab_index+i > 68 && tab_index+i < 81) color = GREEN; 
          else if (tab_index+i > 80 && tab_index < 92) color = RED; 
          else color = WHITE; //Inaczej bialy
            
            if ( (e_moto_logo[tab_index+i]>>j & 1) == 1) pixel(j, i, color, Second_Panel); 
            else pixel(j, i, QUENCH, Second_Panel);
        }
    }
  // Petla dla matrycy pierwszej - po lewej stronie
  for(i=0; i<8; i++)
    {
      for(j = 0; j<16; j++)
        {
          if (tab_index+i+8 > 68 && tab_index+i+8 < 81) color = GREEN; 
          else if (tab_index+i+8 > 80 && tab_index+8 < 92) color = RED; 
          else color = WHITE; 

           if ( (e_moto_logo[tab_index+i+8]>>j & 1) == 1) pixel(j, i, color, First_Panel);
           else pixel(j, i, QUENCH, First_Panel);
        }
    }
 
 if (tab_index >= 92) tab_index = 0;
 else tab_index++;
}


void clear(int nr_panel){
  int i = 0;
  buf[0] = 0x1;
  for(i = 1;i < SIZE;i++){
    buf[i] = 0;
  }
  if(nr_panel == 0) setReg0(0x02,buf,SIZE);
  else if(nr_panel == 1) setReg1(0x02,buf,SIZE);  
  delay(100);
}

void  scroll(unsigned char dir){
  if((dir != None)&&(dir != Right)&&(dir != Left)){
    return;
  }
  if(dir == None){
    buf[0] &= (~(0x01<<2));
  }
  else if(dir == Right){
    buf[0] |= (0x01<<2)|(0x01<<1);
  }
  else if(dir == Left){
    buf[0] |= (0x01<<2);
    buf[0] &= (~(0x01<<1));
  }
  else
    return;
}

void  display(unsigned char picIndex,unsigned char color, int nr_panel){
  
  buf[0] = (buf[0] & (0xe6)) | (0x02 << 3);
  buf[1] = color;
  buf[4] = picIndex;
  if(nr_panel == 0) setReg0(0x02,buf,SIZE);
  else if(nr_panel == 1) setReg1(0x02,buf,SIZE);  
  delay(100);
}

void  print(int val,unsigned char color, int nr_panel){
  String s = String(val);
  print(s,color, nr_panel);
}

void  print(String s,unsigned char color, int nr_panel){
  unsigned char i = 0;
  int len = s.length();
  
  if(len > 40){
    return;
  }
  buf[0] = (buf[0] & (0xe6))| (0x03 << 3) ;
  buf[1] = color;
  for(i = 0;i <= len;i++){
    buf[5+i] = s[i];
  }
  if(nr_panel == 0) setReg0(0x02,buf,SIZE);
  else if(nr_panel == 1) setReg1(0x02,buf,SIZE);  
  delay(100);
}

void  pixel(unsigned char x,unsigned char y,unsigned char color, int nr_panel){
  buf[0] = (buf[0] & (0xe6)) | (0x01 << 3);
  buf[1] = color;
  buf[2] = x;
  buf[3] = y;
  if(nr_panel == 0) setReg0(0x02,buf,SIZE);
  else if(nr_panel == 1) setReg1(0x02,buf,SIZE);  
  delay(1);
}

void  fillScreen(unsigned char color, int nr_panel){
  buf[0] = 0x1;
  buf[0] = buf[0] = (buf[0] & (0xe7)) | (0x01 << 3);
  buf[1] = color;
  buf[2] = 0;
  buf[3] = 0;
  if(nr_panel == 0) setReg0(0x02,buf,SIZE);
  else if(nr_panel == 1) setReg1(0x02,buf,SIZE);  
  delay(100);
}


void  setReg0(unsigned char Reg ,unsigned char *pdata, unsigned char datalen )
{
   Wire.beginTransmission(_RGBAddr); // transmit to device #8
   Wire.write(Reg);              // sends one byte
  
   for(uint8_t i = 0; i < datalen; i++){
      Wire.write(*pdata); 
      pdata++;
    }
 
   Wire.endTransmission();    // stop transmitting
}

int * readReg0(uint8_t addr,uint8_t num){
  static int result[64];
  int i = 0;
  Wire.beginTransmission(_RGBAddr); //Start transmission to device 
  Wire.write(addr); //Sends register address to read rom
  Wire.endTransmission(false); //End transmission
  
  Wire.requestFrom((uint8_t)_RGBAddr, num);//Send data n-bytes read
   while (Wire.available())   // slave may send less than requested
  {
    result[i++] = Wire.read(); // print the character
  }
//  Serial.println(result[0]);
  return result;
}
void  setReg1(unsigned char Reg ,unsigned char *pdata, unsigned char datalen )
{
   Wire1.beginTransmission(_RGBAddr); // transmit to device #8
   Wire1.write(Reg);              // sends one byte
  
   for(uint8_t i = 0; i < datalen; i++){
      Wire1.write(*pdata); 
      pdata++;
    }
 
   Wire1.endTransmission();    // stop transmitting
}

int * readReg1(uint8_t addr,uint8_t num){
  static int result[64];
  int i = 0;
  Wire1.beginTransmission(_RGBAddr); //Start transmission to device 
  Wire1.write(addr); //Sends register address to read rom
  Wire1.endTransmission(false); //End transmission
  
  Wire1.requestFrom((uint8_t)_RGBAddr, num);//Send data n-bytes read
   while (Wire1.available())   // slave may send less than requested
  {
    result[i++] = Wire1.read(); // print the character
  }
//  Serial.println(result[0]);
  return result;
}

void create_e_moto_logo()
{
  int i;
  for (i = 0; i<16; i++) e_moto_logo[i] = 0; 
  /*
   * Battery
  */  
  e_moto_logo[17] = 0x1FFF;
  e_moto_logo[18] = 0x1001;
  e_moto_logo[19] = 0xF041;
  e_moto_logo[20] = 0x91C1;
  e_moto_logo[21] = 0x93F9;
  e_moto_logo[22] = 0x9071;
  e_moto_logo[23] = 0xF041;
  e_moto_logo[24] = 0x1001;
  e_moto_logo[25] = 0x1FFF;
  e_moto_logo[26] = 0x0;
  e_moto_logo[27] = 0x0;
  
  /*
   * E- letter
   */
  
  e_moto_logo[28] = 0xFFFF;
  e_moto_logo[29] = 0xFFFF;
  e_moto_logo[30] = 0xC183;
  e_moto_logo[31] = 0xC183;
  e_moto_logo[32] = 0xC183;
  e_moto_logo[33] = 0xC183;
  e_moto_logo[34] = 0x0;
  e_moto_logo[35] = 0x0;
  
  /*
   *  ,,-" - char 
   */
  e_moto_logo[36] = 0x180;
  e_moto_logo[37] = 0x180;
  e_moto_logo[38] = 0x180;
  e_moto_logo[39] = 0x180;
  e_moto_logo[40] = 0x0;
  e_moto_logo[41] = 0x0;

  /*
   * M - letter
   */
  e_moto_logo[42] = 0xFFFF;
  e_moto_logo[43] = 0xFFFF;
  e_moto_logo[44] = 0x7000;
  e_moto_logo[45] = 0x3800;
    e_moto_logo[46] = 0x1C00;
    e_moto_logo[47] = 0xE00;
    e_moto_logo[48] = 0xE00;
    e_moto_logo[49] = 0x1C00;
    e_moto_logo[50] = 0x3800;
    e_moto_logo[51] = 0x7000;
    e_moto_logo[52] = 0xFFFF;
    e_moto_logo[53] = 0xFFFF;
    e_moto_logo[54] = 0x0;
    e_moto_logo[55] = 0x0;

    /*
     * O - letter
     */
    e_moto_logo[56] = 0x0FF0;
    e_moto_logo[57] = 0x1FF8;
    e_moto_logo[58] = 0x381C;
    e_moto_logo[59] = 0x600E;
    e_moto_logo[60] = 0xE006;
    e_moto_logo[61] = 0xC003;
    e_moto_logo[62] = 0xE006;
    e_moto_logo[63] = 0x600E;
    e_moto_logo[64] = 0x381C;
    e_moto_logo[65] = 0x1FF8;
    e_moto_logo[66] = 0x0FF0;
    e_moto_logo[67] = 0x0;
    e_moto_logo[68] = 0x0;

    /*
     * T - letter
     */
    e_moto_logo[69] = 0xC000;    
    e_moto_logo[70] = 0xC000;
    e_moto_logo[71] = 0xC000;
    e_moto_logo[72] = 0xC000;
    e_moto_logo[73] = 0xFFFF;
    e_moto_logo[74] = 0xFFFF;
    e_moto_logo[75] = 0xC000;
    e_moto_logo[76] = 0xC000;
    e_moto_logo[77] = 0xC000;
    e_moto_logo[78] = 0xC000;
    e_moto_logo[79] = 0x0;
    e_moto_logo[80] = 0x0;

    /*
     * O - letter
     */
    e_moto_logo[81] = 0x0FF0;
    e_moto_logo[82] = 0x1FF8;
    e_moto_logo[83] = 0x381C;
    e_moto_logo[84] = 0x600E;
    e_moto_logo[85] = 0xE006;
    e_moto_logo[86] = 0xC003;
    e_moto_logo[87] = 0xE006;
    e_moto_logo[88] = 0x600E;
    e_moto_logo[89] = 0x381C;
    e_moto_logo[90] = 0x1FF8;
    e_moto_logo[91] = 0x0FF0;
    for(i = 92; i<111; i++)  e_moto_logo[i] = 0x0; 
}
